package it.sevenbits.formatter.formatter;

import it.sevenbits.formatter.formatter.formatter.Formatter;
import it.sevenbits.formatter.formatter.formatter.IFormatter;
import it.sevenbits.formatter.formatter.lexer.LexerFactory;
import it.sevenbits.formatter.formatter.readnwrite.FileReader;
import it.sevenbits.formatter.formatter.readnwrite.FileWriter;

/**
 * Main class of the project
 */
public class Main {
    /**
     * Main method made for simple check
     * @param args - console arguments
     * @throws Exception - exception that throws close method in AutoCloseable IFace
     */
    public static void main(final String[] args) throws Exception {
        try (FileReader reader = new FileReader(args[0]);
            FileWriter writer = new FileWriter(args[1])) {
            IFormatter formatter = new Formatter(new LexerFactory());
            formatter.format(reader, writer);
        }
    }
}
