package it.sevenbits.formatter.formatter.formatter;

import it.sevenbits.formatter.formatter.ITypeDefiner;

import java.util.HashMap;
import java.util.Map;
/**
 * class made for lexeme type definition
 */
public class LexemeTypeDefiner implements ITypeDefiner {
    private Map<String, String> types;
    /**
     * Constructor
     */
    LexemeTypeDefiner() {
        types = new HashMap<>();
        types.put("{", "CURLY_OPEN_BRACKET");
        types.put("}", "CURLY_CLOSE_BRACKET");
        types.put(";", "WORD?_SEMICOLON");
        types.put("\n", "LINE_BREAK");
    }


    @Override
    public String getType(final String word) {
        return types.getOrDefault(word.substring(word.length() - 1), "SIMPLE_LEXEME");
    }
}
