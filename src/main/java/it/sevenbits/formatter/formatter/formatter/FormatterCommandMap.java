package it.sevenbits.formatter.formatter.formatter;

import it.sevenbits.formatter.formatter.Pair;
import it.sevenbits.formatter.formatter.State;
import it.sevenbits.formatter.formatter.formatter.commands.AddLexemeCommand;
import it.sevenbits.formatter.formatter.formatter.commands.AddShiftCommand;
import it.sevenbits.formatter.formatter.formatter.commands.AddSpaceCommand;
import it.sevenbits.formatter.formatter.formatter.commands.IFormatterCommand;
import it.sevenbits.formatter.formatter.formatter.commands.IncreaseShiftCommand;
import it.sevenbits.formatter.formatter.formatter.commands.DecreaseShiftCommand;
import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Command map made for formatter automate
 */
public class FormatterCommandMap {
    private final Map<Pair<State, String>, List<IFormatterCommand>> commands;
    /**
     * Constructor
     * @param writer - place where to write the result
     * @param openBrackets - num of open brackets
     * @param lexeme - lexeme that has to be processed
     */
    FormatterCommandMap(final IWriter writer, final Counter openBrackets, final String lexeme) {
        commands = new HashMap<>();
        State listenState = new State("USUAL_LISTEN");
        State firstListen = new State("FIRST_LISTEN");

        List<IFormatterCommand> forLexeme = new ArrayList<>();
        forLexeme.add(new AddShiftCommand(openBrackets, writer));
        forLexeme.add(new AddLexemeCommand(writer, lexeme));
        forLexeme.add(new AddSpaceCommand(writer));

        List<IFormatterCommand> forOpenBracket = new ArrayList<>();
        forOpenBracket.add(new AddShiftCommand(openBrackets, writer));
        forOpenBracket.add(new IncreaseShiftCommand(openBrackets));
        forOpenBracket.add(new AddLexemeCommand(writer, lexeme));


        List<IFormatterCommand> forCloseBracket = new ArrayList<>();
        forCloseBracket.add(new DecreaseShiftCommand(openBrackets));
        forCloseBracket.add(new AddShiftCommand(openBrackets, writer));
        forCloseBracket.add(new AddLexemeCommand(writer, lexeme));


        List<IFormatterCommand> forSemicolon = new ArrayList<>();
        forSemicolon.add(new AddShiftCommand(openBrackets, writer));
        forSemicolon.add(new AddLexemeCommand(writer, lexeme));


        List<IFormatterCommand> forLineBreak = new ArrayList<>();
        forLineBreak.add(new AddLexemeCommand(writer, lexeme));

        this.commands.put(new Pair<>(firstListen, "SIMPLE_LEXEME"), forLexeme);
        this.commands.put(new Pair<>(firstListen, "LINE_BREAK"), forLineBreak);
        this.commands.put(new Pair<>(firstListen, "CURLY_OPEN_BRACKET"), forOpenBracket);
        this.commands.put(new Pair<>(firstListen, "CURLY_CLOSE_BRACKET"), forCloseBracket);
        this.commands.put(new Pair<>(firstListen, "WORD?_SEMICOLON"), forSemicolon);

        forLexeme = new ArrayList<>();
        forLexeme.add(new AddLexemeCommand(writer, lexeme));
        forLexeme.add(new AddSpaceCommand(writer));

        List<IFormatterCommand> forOpenBracketListen = new ArrayList<>(forOpenBracket);
        forOpenBracketListen.remove(0);

        List<IFormatterCommand> forCloseBracketListen = new ArrayList<>(forCloseBracket);
        forCloseBracketListen.remove(1);

        List<IFormatterCommand> forSemicolonListen = new ArrayList<>(forSemicolon);
        forSemicolonListen.remove(0);

        this.commands.put(new Pair<>(listenState, "LINE_BREAK"), new ArrayList<>());
        this.commands.put(new Pair<>(listenState, "SIMPLE_LEXEME"), forLexeme);
        this.commands.put(new Pair<>(listenState, "CURLY_OPEN_BRACKET"), forOpenBracketListen);
        this.commands.put(new Pair<>(listenState, "CURLY_CLOSE_BRACKET"), forCloseBracketListen);
        this.commands.put(new Pair<>(listenState, "WORD?_SEMICOLON"), forSemicolonListen);
    }
    /**
     * method that returns command for pair: state and type
     * @param state - state of the formatter automate
     * @param type - type of lexeme
     * @return list of commands
     */
    public List<IFormatterCommand> getCommand(final State state, final String type) {
        return commands.getOrDefault(new Pair<>(state, type), new ArrayList<>());
    }
}
