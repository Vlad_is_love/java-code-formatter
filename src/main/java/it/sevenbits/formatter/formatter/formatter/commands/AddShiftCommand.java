package it.sevenbits.formatter.formatter.formatter.commands;

import it.sevenbits.formatter.formatter.formatter.Counter;
import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.io.IOException;
/**
 * Command that adds shift at the start of the line
 */
public class AddShiftCommand implements IFormatterCommand {
    private final Counter openBrackets;
    private final IWriter writer;
    /**
     * Constructor
     * @param writer - place where to add shift
     * @param openBrackets - num of open brackets in code
     */
    public AddShiftCommand(final Counter openBrackets, final IWriter writer) {
        this.openBrackets = openBrackets;
        this.writer = writer;
    }

    @Override
    public void execute() throws IOException {
        for (int i = 0; i < openBrackets.getCounter(); i++) {
            writer.write("    ");
        }
    }
}
