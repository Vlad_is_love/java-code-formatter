package it.sevenbits.formatter.formatter.formatter;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import it.sevenbits.formatter.formatter.Pair;
import it.sevenbits.formatter.formatter.State;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * class made for formatter state transitions
 */
public class FormatterStateMap {
    private final State firstListen = new State("FIRST_LISTEN");

    private final Map<Pair<State, String>, State> states;
    /**
     * Constructor
     */
    FormatterStateMap() throws IOException {
        states = new HashMap<>();
        State listenState = new State("USUAL_LISTEN");
        State commentState = new State("COMMENT");

        List<Map<String, String>> response = new LinkedList<>();
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        MappingIterator<Map<String, String>> iterator = mapper.reader(Map.class)
                .with(schema)
                .readValues(new File("src/main/resources/config/formatterStateMap.csv"));
        while (iterator.hasNext()) {
            response.add(iterator.nextValue());
        }

        Map<String, State> stringToStateMap = new HashMap<>();
        stringToStateMap.put("firstListen", firstListen);
        stringToStateMap.put("listenState", listenState);
        stringToStateMap.put("commentState", commentState);

        for (Map<String, String> map : response) {
            states.put(new Pair<>(stringToStateMap.get(map.get("previousState")), map.get("lexeme")),
                    stringToStateMap.get(map.get("nextState")));
        }
    }

    public State getStartState() {
        return firstListen;
    }
    /**
     * method that returns next state for pair: state and type
     * @param state - state of the formatter automate
     * @param type - type of lexeme
     * @return next state
     */
    public State getNextState(final State state, final String type) {
        return states.getOrDefault(new Pair<>(state, type), firstListen);
    }
}