package it.sevenbits.formatter.formatter.formatter;

import it.sevenbits.formatter.formatter.State;
import it.sevenbits.formatter.formatter.formatter.commands.IFormatterCommand;
import it.sevenbits.formatter.formatter.lexer.ILexer;
import it.sevenbits.formatter.formatter.lexer.ILexerFactory;
import it.sevenbits.formatter.formatter.lexer.IToken;
import it.sevenbits.formatter.formatter.readnwrite.IReader;
import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.io.IOException;
import java.util.List;

/**
 * Main class for code formatter
 */
public class Formatter implements IFormatter {
    private final ILexerFactory lexerFactory;
    /**
     * Constructor for the class
     * @param lexerFactory - factory from which we will get lexer
     */
    public Formatter(final ILexerFactory lexerFactory) {
        this.lexerFactory = lexerFactory;
    }

    @Override
    public void format(final IReader reader, final IWriter writer) throws IOException {
        ILexer lexer = lexerFactory.createLexer(reader);
        Counter openBracketsCounter = new Counter();
        FormatterStateMap formatterStateMap = new FormatterStateMap();
        State currentState = formatterStateMap.getStartState();
        while (lexer.hasMoreTokens()) {
            IToken token = lexer.readToken();
            String lexeme = token.getLexeme();
            LexemeTypeDefiner lexemeTypeDefiner = new LexemeTypeDefiner();
            String lexemeType = lexemeTypeDefiner.getType(lexeme);
            FormatterCommandMap formatterCommandMap = new FormatterCommandMap(writer, openBracketsCounter, lexeme);
            List<IFormatterCommand> commands;
            commands = formatterCommandMap.getCommand(currentState, lexemeType);
            for (IFormatterCommand command : commands) {
                command.execute();
            }
            currentState = formatterStateMap.getNextState(currentState, lexemeType);
        }
    }
}
