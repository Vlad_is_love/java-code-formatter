package it.sevenbits.formatter.formatter.formatter.commands;

import java.io.IOException;
/**
 * IFace for commands for formatter
 */
public interface IFormatterCommand {
    /**
     * method that executes instructions written in command
     * @throws IOException - exception which throws from every JavaIO method
     */
    void execute() throws IOException;
}
