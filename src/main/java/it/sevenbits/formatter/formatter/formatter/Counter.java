package it.sevenbits.formatter.formatter.formatter;
/**
 * counter class made as a wrapper for int type
 */
public class Counter {
    private int counter;
    /**
     * Constructor
     */
    public Counter() {
        this.counter = 0;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(final int counter) {
        this.counter = counter;
    }
    /**
     * method that increases the value
     */
    public void increase() {
        counter++;
    }
    /**
     * method that decreases the value
     */
    public void decrease() {
        counter--;
    }
}
