package it.sevenbits.formatter.formatter.formatter.commands;

import it.sevenbits.formatter.formatter.formatter.Counter;

import java.io.IOException;
/**
 * Command that decreases number of openedBrackets
 */
public class DecreaseShiftCommand implements IFormatterCommand {
    private final Counter openBrackets;

    /**
     * Constructor
     * @param openBrackets - number of open brackets
     */
    public DecreaseShiftCommand(final Counter openBrackets) {
        this.openBrackets = openBrackets;
    }

    @Override
    public void execute() throws IOException {
        openBrackets.decrease();
    }
}
