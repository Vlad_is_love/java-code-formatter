package it.sevenbits.formatter.formatter.formatter.commands;

import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.io.IOException;
/**
 * Command that adds space
 */
public class AddSpaceCommand implements IFormatterCommand {
    private final IWriter writer;

    /**
     * Constructor
     * @param writer - place where to add space
     */
    public AddSpaceCommand(final IWriter writer) {
        this.writer = writer;
    }

    @Override
    public void execute() throws IOException {
        writer.write(" ");
    }
}
