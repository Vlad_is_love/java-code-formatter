package it.sevenbits.formatter.formatter.formatter.commands;

import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.io.IOException;
/**
 * Command that writes lexeme
 */
public class AddLexemeCommand implements IFormatterCommand {
    private final IWriter writer;
    private final String lexeme;

    /**
     * Constructor
     * @param writer - place where to write
     * @param lexeme - lexeme that has to be written
     */
    public AddLexemeCommand(final IWriter writer, final String lexeme) {
        this.writer = writer;
        this.lexeme = lexeme;
    }

    @Override
    public void execute() throws IOException {
        this.writer.write(lexeme);
    }
}
