package it.sevenbits.formatter.formatter.formatter;

import it.sevenbits.formatter.formatter.readnwrite.IReader;
import it.sevenbits.formatter.formatter.readnwrite.IWriter;

import java.io.IOException;
/**
 * IFace for Formatters which performs code formation
 */
public interface IFormatter {
    /**
     * Method that formats code form reader and writes the result in writer
     * @param reader - method's place to read
     * @param writer - method's place to write
     * @throws IOException - exception that could happen in work with Input or Output Stream
     */
    void format(IReader reader, IWriter writer) throws IOException;
}
