package it.sevenbits.formatter.formatter.formatter.commands;

import it.sevenbits.formatter.formatter.formatter.Counter;
import java.io.IOException;
/**
 * Command that increases number of openedBrackets
 */
public class IncreaseShiftCommand implements IFormatterCommand {
    private final Counter openBrackets;

    /**
     * Constructor
     * @param openBrackets - number of open brackets
     */
    public IncreaseShiftCommand(final Counter openBrackets) {
        this.openBrackets = openBrackets;
    }

    @Override
    public void execute() throws IOException {
        System.out.println();
        openBrackets.increase();
    }
}
