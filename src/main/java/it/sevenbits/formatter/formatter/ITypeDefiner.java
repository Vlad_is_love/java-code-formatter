package it.sevenbits.formatter.formatter;
/**
 * Iface for type definers
 */
public interface ITypeDefiner {
    /**
     * method that returns type of word
     * @param word - word which type has to be returned
     * @return - type of word
     */
    String getType(String word);
}
