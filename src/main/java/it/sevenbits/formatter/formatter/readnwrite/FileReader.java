package it.sevenbits.formatter.formatter.readnwrite;

import java.io.BufferedReader;
import java.io.IOException;
/**
 * IReader realisation that reads from file
 */
public class FileReader implements IReader, AutoCloseable {
    private final BufferedReader reader;
    /**
     * Constructor
     * @param fileName - file name form which we will read
     * @throws IOException - exception which throws from every JavaIO method
     */
    public FileReader(final String fileName) throws IOException {
        reader = new BufferedReader(new java.io.FileReader(fileName));
    }

    @Override
    public boolean hasNext() throws IOException {
        return reader.ready();
    }

    @Override
    public char read() throws IOException {
        return (char) reader.read();
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}
