package it.sevenbits.formatter.formatter.readnwrite;
/**
 * Class that does character output in string
 */
public class StringWriter implements IWriter {
    private final StringBuilder stringBuilder;
    /**
     * Constructor for the class
     * @param string - string in which we want to write
     */
    public StringWriter(final String string) {
        this.stringBuilder = new StringBuilder(string);
    }

    @Override
    public void write(final char c) {
        stringBuilder.append(c);
    }

    @Override
    public void write(final String str) {
        for (int i = 0; i < str.length(); i++) {
            this.write(str.charAt(i));
        }
    }

    public String getString() {
        return stringBuilder.toString();
    }
}
