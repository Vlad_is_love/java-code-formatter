package it.sevenbits.formatter.formatter.readnwrite;

import java.io.IOException;
/**
 * Interface for classes that could write in some stream
 */
public interface IWriter {
    /**
     * Method that writes character in some stream
     * @param c - character that should be written
     * @throws IOException - exception that could happen in work with Input or Output Stream
     */
    void write(char c) throws IOException;
    /**
     * Method that writes string in some stream
     * @param str - string that should be written
     * @throws IOException - exception that could happen in work with Input or Output Stream
     */
    void write(String str) throws IOException;
}
