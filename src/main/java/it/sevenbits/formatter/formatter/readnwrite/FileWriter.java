package it.sevenbits.formatter.formatter.readnwrite;

import java.io.BufferedWriter;
import java.io.IOException;
/**
 * IWriter realisation that writes to file
 */
public class FileWriter implements IWriter, AutoCloseable {
    private final BufferedWriter writer;
    /**
     * Constructor
     * @param fileName - file name in which we will write
     * @throws IOException - exception which throws from every JavaIO method
     */
    public FileWriter(final String fileName) throws IOException {
        this.writer = new BufferedWriter(new java.io.FileWriter(fileName));
    }

    @Override
    public void write(final char c) throws IOException {
        writer.write(c);
    }

    @Override
    public void write(final String str) throws IOException {
        writer.write(str);
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }
}
