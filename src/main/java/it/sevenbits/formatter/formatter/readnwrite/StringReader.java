package it.sevenbits.formatter.formatter.readnwrite;
/**
 * Class that does character input from string
 */
public class StringReader implements IReader {

    private final String inputString;
    private int currentElementNumber;
    /**
     * Constructor for the class
     * @param inputString - string form which we are going to read
     */
    public StringReader(final String inputString) {
        this.inputString = inputString;
    }

    @Override
    public boolean hasNext() {
        return currentElementNumber < inputString.length();
    }

    @Override
    public char read() {
        return inputString.charAt(currentElementNumber++);
    }
}
