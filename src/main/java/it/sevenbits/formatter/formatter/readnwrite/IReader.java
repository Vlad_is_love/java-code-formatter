package it.sevenbits.formatter.formatter.readnwrite;

import java.io.IOException;
/**
 * Interface for classes that could read from some stream
 */
public interface IReader {
    /**
     * Method that checks if there is any more character
     * @throws IOException - exception that could happen in work with Input or Output Stream
     * @return - if there is one more or not
     */
    boolean hasNext() throws IOException;
    /**
     * Method that reads from the stream
     * @throws IOException - exception that could happen in work with Input or Output Stream
     * @return - char that was read from stream
     */
    char read() throws IOException;
}
