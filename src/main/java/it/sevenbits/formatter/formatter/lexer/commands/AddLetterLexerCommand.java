package it.sevenbits.formatter.formatter.lexer.commands;

import it.sevenbits.formatter.formatter.lexer.Letter;
/**
 * Command that adds letter to lexeme
 */
public class AddLetterLexerCommand implements ILexerCommand {
    private final StringBuilder lexeme;
    private final Letter letter;
    /**
     * Constructor
     * @param lexeme - place where to add
     * @param letter - the letter which is need to be added
     */
    public AddLetterLexerCommand(final StringBuilder lexeme, final Letter letter) {
        this.lexeme = lexeme;
        this.letter = letter;
    }

    @Override
    public void execute() {
        lexeme.append(letter.getSense());
    }
}
