package it.sevenbits.formatter.formatter.lexer;
/**
 * class for letter containing it's type and string representation
 */
public class Letter {
    private final String type;
    private final String sense;
    /**
     * Constructor
     * @param sense - string representation of letter
     */
    public Letter(final String sense) {
        this.sense = sense;
        LetterMap letterMap = new LetterMap();
        type = letterMap.getType(sense);
    }

    public String getType() {
        return type;
    }

    public String getSense() {
        return sense;
    }
}
