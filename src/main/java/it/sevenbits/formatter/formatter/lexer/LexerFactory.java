package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.lexer.ILexer;
import it.sevenbits.formatter.formatter.lexer.ILexerFactory;
import it.sevenbits.formatter.formatter.lexer.Lexer;
import it.sevenbits.formatter.formatter.readnwrite.IReader;

import java.io.IOException;
/**
 * LexerFactory that creates lexers
 */
public class LexerFactory implements ILexerFactory {
    @Override
    public ILexer createLexer(final IReader reader) throws IOException {
        return new Lexer(reader);
    }
}
