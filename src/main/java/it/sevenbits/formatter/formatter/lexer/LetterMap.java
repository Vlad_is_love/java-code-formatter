package it.sevenbits.formatter.formatter.lexer;


import it.sevenbits.formatter.formatter.ITypeDefiner;

import java.util.HashMap;
import java.util.Map;
/**
 * class made for type definition of the letter
 */
public class LetterMap implements ITypeDefiner {
    private final Map<String, String> types;
    /**
     * Constructor
     */
    LetterMap() {
        types = new HashMap<>();
        types.put(" ", "SPACE");
        types.put(";", "SEMICOLON");
        types.put("\n", "LINE_BREAK");
        types.put("{", "CURLY_BRACKET");
        types.put("}", "CURLY_BRACKET");
        types.put("\'", "QUOTES");
        types.put("\"", "QUOTES");
        types.put("/", "COMMENT");
    }

    @Override
    public String getType(final String sense) {
        return types.getOrDefault(sense, "SYMBOL");
    }
}
