package it.sevenbits.formatter.formatter.lexer;

import java.io.IOException;
/**
 * IFace for lexers which extracts tokens from code
 */
public interface ILexer {
    /**
     * Get next token from the non formatted code
     * @throws IOException - exception that could happen in work with Input or Output Stream
     * @return IToken - token extracted from code
     */
    IToken readToken() throws IOException;
    /**
     * Tells if there are more tokens or not
     * @throws IOException - exception that could happen in work with Input or Output Stream
     * @return boolean - if there are more tokens or not
     */
    boolean hasMoreTokens() throws IOException;
}
