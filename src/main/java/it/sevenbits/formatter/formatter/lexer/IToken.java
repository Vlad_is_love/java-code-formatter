package it.sevenbits.formatter.formatter.lexer;

/**
 * IFace for tokens that contains name and lexeme
 */
public interface IToken {
    /**
     * returns token's name
     * @return String name
     */
    String getName();
    /**
     * returns token's lexeme
     * @return String lexeme
     */
    String getLexeme();
}
