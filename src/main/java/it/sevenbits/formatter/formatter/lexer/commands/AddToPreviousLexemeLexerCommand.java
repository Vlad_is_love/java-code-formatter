package it.sevenbits.formatter.formatter.lexer.commands;

import it.sevenbits.formatter.formatter.lexer.Letter;

import java.util.List;
/**
 * Command that adds letter to previous lexeme in line
 */
public class AddToPreviousLexemeLexerCommand implements ILexerCommand {
    private final StringBuilder lexeme;
    private final List<String> lexemsLine;
    private final Letter letter;
    /**
     * Constructor
     * @param lexeme - place where to add
     * @param lexemsLine - list with lexems in one line
     * @param letter - letter that has to be added
     */
    public AddToPreviousLexemeLexerCommand(final StringBuilder lexeme, final List<String> lexemsLine, final Letter letter) {
        this.lexeme = lexeme;
        this.lexemsLine = lexemsLine;
        this.letter = letter;
    }

    @Override
    public void execute() {
        String temp = lexemsLine.get(lexemsLine.size() - 1);
        temp += letter.getSense();
        lexemsLine.remove(lexemsLine.size() - 1);
        lexemsLine.add(temp);
    }
}
