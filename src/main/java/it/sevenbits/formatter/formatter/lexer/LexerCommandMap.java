package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.Pair;
import it.sevenbits.formatter.formatter.State;
import it.sevenbits.formatter.formatter.lexer.commands.AddLetterLexerCommand;
import it.sevenbits.formatter.formatter.lexer.commands.AddToPreviousLexemeLexerCommand;
import it.sevenbits.formatter.formatter.lexer.commands.FinishLexemeLexerCommand;
import it.sevenbits.formatter.formatter.lexer.commands.AddLineBreakLexerCommand;
import it.sevenbits.formatter.formatter.lexer.commands.ILexerCommand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Command map made for lexer automate
 */
public class LexerCommandMap {
    private final Map<Pair<State, String>, List<ILexerCommand>> commands;
    /**
     * Constructor
     * @param lexeme - the lexeme which is creating
     * @param lexemsLine - list with lexemes in on line
     * @param letter - current letter
     */
    public LexerCommandMap(final StringBuilder lexeme, final List<String> lexemsLine, final Letter letter) {
        this.commands = new HashMap<>();
        State listenState = new State("LISTEN");
        State defaultState = new State("SUS_SPACE");
        State endState = new State("END_STATE");
        State finishState = new State("FINISH_STATE");
        State commentState = new State("COMMENT_STATE");
        State stringLiter = new State("STRING_LITERAL");

        List<ILexerCommand> forSymbol = new ArrayList<>();
        forSymbol.add(new AddLetterLexerCommand(lexeme, letter));

        List<ILexerCommand> forSemicolon = new ArrayList<>();
        forSemicolon.add(new AddToPreviousLexemeLexerCommand(lexeme, lexemsLine, letter));

        List<ILexerCommand> forCurlyBracketAndSemicolon = new ArrayList<>();
        forCurlyBracketAndSemicolon.add(new AddLetterLexerCommand(lexeme, letter));
        forCurlyBracketAndSemicolon.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        List<ILexerCommand> forLineBreak = new ArrayList<>();
        forLineBreak.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));
        forLineBreak.add(new AddLetterLexerCommand(lexeme, letter));
        forLineBreak.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        this.commands.put(new Pair<>(defaultState, "SPACE"), new ArrayList<>());
        this.commands.put(new Pair<>(defaultState, "LINE_BREAK"), new ArrayList<>());
        this.commands.put(new Pair<>(defaultState, "SYMBOL"), forSymbol);
        this.commands.put(new Pair<>(defaultState, "SEMICOLON"), forSemicolon);
        this.commands.put(new Pair<>(defaultState, "CURLY_BRACKET"), forCurlyBracketAndSemicolon);
        this.commands.put(new Pair<>(defaultState, "QUOTES"), forSymbol);

        List<ILexerCommand> forSpace = new ArrayList<>();
        forSpace.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        List<ILexerCommand> forCurlyBracket = new ArrayList<>();
        forCurlyBracket.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));
        forCurlyBracket.add(new AddLetterLexerCommand(lexeme, letter));
        forCurlyBracket.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        List<ILexerCommand> forQuotes = new ArrayList<>();
        forQuotes.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));
        forQuotes.add(new AddLetterLexerCommand(lexeme, letter));

        this.commands.put(new Pair<>(listenState, "SPACE"), forSpace);
        this.commands.put(new Pair<>(listenState, "LINE_BREAK"), forLineBreak);
        this.commands.put(new Pair<>(listenState, "SYMBOL"), forSymbol);
        this.commands.put(new Pair<>(listenState, "SEMICOLON"), forCurlyBracketAndSemicolon);
        this.commands.put(new Pair<>(listenState, "CURLY_BRACKET"), forCurlyBracket);
        this.commands.put(new Pair<>(listenState, "QUOTES"), forQuotes);

        this.commands.put(new Pair<>(stringLiter, "QUOTES"), forCurlyBracketAndSemicolon);
        this.commands.put(new Pair<>(stringLiter, "SPACE"), forSymbol);
        this.commands.put(new Pair<>(stringLiter, "LINE_BREAK"), forSymbol);
        this.commands.put(new Pair<>(stringLiter, "SYMBOL"), forSymbol);
        this.commands.put(new Pair<>(stringLiter, "SEMICOLON"), forSymbol);
        this.commands.put(new Pair<>(stringLiter, "CURLY_BRACKET"), forSymbol);

        List<ILexerCommand> toEnd = new ArrayList<>();
        toEnd.add(new AddLineBreakLexerCommand(lexeme));
        toEnd.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));
        toEnd.add(new AddLetterLexerCommand(lexeme, letter));

        List<ILexerCommand> toEndOneSymbol = new ArrayList<>(toEnd);
        toEndOneSymbol.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));
        toEndOneSymbol.add(new AddLineBreakLexerCommand(lexeme));
        toEndOneSymbol.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        List<ILexerCommand> forLineBreakToEnd = new ArrayList<>();
        forLineBreakToEnd.add(new AddLineBreakLexerCommand(lexeme));
        forLineBreakToEnd.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        this.commands.put(new Pair<>(finishState, "QUOTES"), toEnd);
        this.commands.put(new Pair<>(finishState, "SPACE"), forLineBreakToEnd);
        this.commands.put(new Pair<>(finishState, "LINE_BREAK"), forLineBreakToEnd);
        this.commands.put(new Pair<>(finishState, "SYMBOL"), toEnd);
        this.commands.put(new Pair<>(finishState, "SEMICOLON"), toEnd);
        this.commands.put(new Pair<>(finishState, "CURLY_BRACKET"), toEndOneSymbol);
        this.commands.put(new Pair<>(finishState, "COMMENT"), forSymbol);

        List<ILexerCommand> fromCommentToEnd = new ArrayList<>();
        fromCommentToEnd.add(new AddLineBreakLexerCommand(lexeme));
        fromCommentToEnd.add(new FinishLexemeLexerCommand(lexeme, lexemsLine));

        this.commands.put(new Pair<>(commentState, "QUOTES"), forSymbol);
        this.commands.put(new Pair<>(commentState, "SPACE"), forSymbol);
        this.commands.put(new Pair<>(commentState, "LINE_BREAK"), fromCommentToEnd);
        this.commands.put(new Pair<>(commentState, "SYMBOL"), forSymbol);
        this.commands.put(new Pair<>(commentState, "SEMICOLON"), forSymbol);
        this.commands.put(new Pair<>(commentState, "CURLY_BRACKET"), forSymbol);
        this.commands.put(new Pair<>(commentState, "COMMENT"), forSymbol);
    }
    /**
     * method that returns command for pair: state and type
     * @param state - state of the lexer automate
     * @param letter - letter
     * @return list of commands
     */
    public List<ILexerCommand> getCommand(final State state, final Letter letter) {
        return commands.getOrDefault(new Pair<>(state, letter.getType()), new ArrayList<>());
    }
}
