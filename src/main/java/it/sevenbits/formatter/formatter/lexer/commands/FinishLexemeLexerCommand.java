package it.sevenbits.formatter.formatter.lexer.commands;

import java.util.List;
/**
 * Command that finishes lexeme
 */
public class FinishLexemeLexerCommand implements ILexerCommand {
    private final StringBuilder lexeme;
    private final List<String> lexemsLine;
    /**
     * Constructor
     * @param lexeme - lexeme that has to be finished
     * @param lexemsLine - list with lexems in one line
     */
    public FinishLexemeLexerCommand(final StringBuilder lexeme, final List<String> lexemsLine) {
        this.lexeme = lexeme;
        this.lexemsLine = lexemsLine;
    }

    @Override
    public void execute() {
        lexemsLine.add(lexeme.toString());
        lexeme.setLength(0);
    }
}
