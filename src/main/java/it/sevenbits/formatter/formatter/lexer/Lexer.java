package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.State;
import it.sevenbits.formatter.formatter.lexer.commands.ILexerCommand;
import it.sevenbits.formatter.formatter.readnwrite.IReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Lexer that extracts tokens from code
 */
public class Lexer implements ILexer {
    private final IReader reader;
    private List<String> lexemsLine;
    private final StringBuilder sb;
    private int tokensGiven;
    /**
     * Constructor
     * @param reader - reader from which lexer will read
     * @throws IOException - exception that could happen in work with Input or Output Stream
     */
    public Lexer(final IReader reader) throws IOException {
        this.reader = reader;
        tokensGiven = 0;
        sb = new StringBuilder();
        readLexemeLine();
    }

    private void readLexemeLine() throws IOException {
        lexemsLine = new ArrayList<>();
        LexerStateMap lexerStateMap = new LexerStateMap();
        State currentState = lexerStateMap.getStartState();
        while (reader.hasNext() && !currentState.equals(new State("END_STATE"))) {
            char rawLetter = reader.read();
            StringBuilder temp = new StringBuilder();
            temp.append(rawLetter);
            Letter letter = new Letter(temp.toString());
            LexerCommandMap commandMap = new LexerCommandMap(sb, lexemsLine, letter);
            List<ILexerCommand> commands = commandMap.getCommand(currentState, letter);
            for (ILexerCommand comm : commands) {
                comm.execute();
            }
            currentState = lexerStateMap.getNextState(currentState, letter);
        }
        lexemsLine.remove("");
    }

    @Override
    public IToken readToken() throws IOException {
        if (tokensGiven == lexemsLine.size()) {
            hasMoreTokens();
        }
        return new Token("", lexemsLine.get(tokensGiven++));
    }

    @Override
    public boolean hasMoreTokens() throws IOException {
        if (tokensGiven == lexemsLine.size()) {
            do {
                readLexemeLine();
                if (lexemsLine.size() != 0) {
                    break;
                }
            } while (reader.hasNext());
            tokensGiven = 0;
            return lexemsLine.size() != 0;
        } else {
            return true;
        }
    }
}
