package it.sevenbits.formatter.formatter.lexer.commands;

/**
 * IFace for lexer's commands
 */
public interface ILexerCommand {
    /**
     * method that executes instructions written in command
     */
    void execute();
}
