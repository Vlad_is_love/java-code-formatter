package it.sevenbits.formatter.formatter.lexer.commands;
/**
 * Command that adds line break to the lexeme
 */
public class AddLineBreakLexerCommand implements ILexerCommand {
    private final StringBuilder lexeme;
    /**
     * Constructor
     * @param lexeme - place where to add
     */
    public AddLineBreakLexerCommand(final StringBuilder lexeme) {
        this.lexeme = lexeme;
    }

    @Override
    public void execute() {
        lexeme.append("\n");
    }
}
