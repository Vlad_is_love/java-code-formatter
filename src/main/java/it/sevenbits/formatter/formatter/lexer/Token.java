package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.lexer.IToken;

/**
 * Token that have its name and lexeme
 */
public class Token implements IToken {
    private final String name;
    private final String lexeme;
    /**
     * Constructor
     * @param name - token's name
     * @param lexeme - token's lexeme
     */
    public Token(final String name, final String lexeme) {
        this.name = name;
        this.lexeme = lexeme;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getLexeme() {
        return lexeme;
    }
}
