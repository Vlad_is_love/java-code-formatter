package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.lexer.ILexer;
import it.sevenbits.formatter.formatter.readnwrite.IReader;

import java.io.IOException;
/**
 * IFace for lexer factories which is needed to return lexers
 */
public interface ILexerFactory {
    /**
     * creates lexer using given reader
     * @param reader - reader from which created lexer will read
     * @throws IOException - exception that could happen in work with Input or Output Stream
     * @return ILexer - the lexer itself
     */
    ILexer createLexer(IReader reader) throws IOException;
}
