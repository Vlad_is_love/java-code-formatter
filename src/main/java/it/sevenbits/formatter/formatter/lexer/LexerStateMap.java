package it.sevenbits.formatter.formatter.lexer;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import it.sevenbits.formatter.formatter.Pair;
import it.sevenbits.formatter.formatter.State;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * class made for lexer state transitions
 */
public class LexerStateMap {
    private final State defaultState = new State("SUS_SPACE");

    private final Map<Pair<State, String>, State> states;
    /**
     * Constructor
     */
    LexerStateMap() throws IOException {
        states = new HashMap<>();
        State listenState = new State("LISTEN");
        State finishState = new State("FINISH_STATE");
        State stringLiter = new State("STRING_LITERAL");
        State endState = new State("END_STATE");
        State commentState = new State("COMMENT_STATE");

        List<Map<String, String>> response = new LinkedList<>();
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        MappingIterator<Map<String, String>> iterator = mapper.reader(Map.class)
                .with(schema)
                .readValues(new File("src/main/resources/config/lexerStateMap.csv"));
        while (iterator.hasNext()) {
            response.add(iterator.nextValue());
        }

        Map<String, State> stringToStateMap = new HashMap<>();
        stringToStateMap.put("defaultState", defaultState);
        stringToStateMap.put("listenState", listenState);
        stringToStateMap.put("stringLiter", stringLiter);
        stringToStateMap.put("finishState", finishState);
        stringToStateMap.put("commentState", commentState);
        stringToStateMap.put("endState", endState);

        for (Map<String, String> map : response) {
            states.put(new Pair<>(stringToStateMap.get(map.get("previousState")), map.get("lexeme")),
                    stringToStateMap.get(map.get("nextState")));
        }
    }

    public State getStartState() {
        return defaultState;
    }
    /**
     * method that returns next state for pair: state and letter type
     * @param state - state of the formatter automate
     * @param letter - letter
     * @return next state
     */
    public State getNextState(final State state, final Letter letter) {
        return states.getOrDefault(new Pair<>(state, letter.getType()), defaultState);
    }

}
