package it.sevenbits.formatter.formatter;

import java.util.Objects;
/**
 * State for automate
 */
public class State {
    private final String currentState;
    /**
     * Constructor
     * @param currentState - current value of state
     */
    public State(final String currentState) {
        this.currentState = currentState;
    }
    /**
     * method that returns string representation of state
     * @return string representation of state
     */
    public String toString() {
        return currentState;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        State state = (State) o;
        return Objects.equals(currentState, state.currentState);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentState);
    }
}

