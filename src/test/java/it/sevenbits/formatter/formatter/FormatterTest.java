package it.sevenbits.formatter.formatter;

import it.sevenbits.formatter.formatter.formatter.Formatter;
import it.sevenbits.formatter.formatter.lexer.LexerFactory;
import it.sevenbits.formatter.formatter.readnwrite.StringReader;
import it.sevenbits.formatter.formatter.readnwrite.StringWriter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FormatterTest {

    private Formatter formatter;
    private StringReader stringReader;
    private StringWriter stringWriter;

    @Before
    public void setUp() {
        formatter = new Formatter(new LexerFactory());
        stringWriter = new StringWriter("");
    }

    @Test
    public void testFormatterSimpleCodeSequence() throws IOException {
        stringReader = new StringReader("{{{{}}}}");
        formatter.format(stringReader, stringWriter);
        String expectedResult = "{\n    {\n" +
                "        {\n" +
                "            {\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}\n";
        assertEquals("wrong result", expectedResult, stringWriter.getString());
    }

    @Test
    public void testFormatterCodeWrittenInLine() throws IOException {
        stringReader = new StringReader("public interface IReader { boolean hasNext() throws IOException;// comment\n char read() throws IOException;}");
        formatter.format(stringReader, stringWriter);
        String expectedResult = "public interface IReader {\n" +
                "    boolean hasNext() throws IOException;// comment\n" +
                "    char read() throws IOException;\n" +
                "}\n";
        assertEquals("wrong result", expectedResult, stringWriter.getString());
    }

    @Test
    public void testFormatterCodeWithExtraWhitespacesAndNewLines() throws IOException {
        stringReader = new StringReader("public class Animal {\n" +
                "    int age;\n" +
                "    \n" +
                "    \n" +
                "    \n" +
                "    public       Animal(int age) {\n" +
                "        this.age = age;\n" +
                "    }\n" +
                "    public int        getAge() {\n" +
                "             return age;\n" +
                "    }\n" +
                "}");
        formatter.format(stringReader, stringWriter);
        String expectedResult = "public class Animal {\n" +
                "    int age;\n" +
                "    public Animal(int age) {\n" +
                "        this.age = age;\n" +
                "    }\n" +
                "    public int getAge() {\n" +
                "        return age;\n" +
                "    }\n" +
                "}";
        assertEquals("wrong result", expectedResult, stringWriter.getString());
    }
}
