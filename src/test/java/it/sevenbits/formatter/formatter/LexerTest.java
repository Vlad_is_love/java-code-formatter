package it.sevenbits.formatter.formatter;

import it.sevenbits.formatter.formatter.lexer.Lexer;
import it.sevenbits.formatter.formatter.readnwrite.FileReader;
import it.sevenbits.formatter.formatter.readnwrite.IReader;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LexerTest {

    @Test
    public void testReadTokenForTwoSimpleLexems() throws Exception {
        try(FileReader reader = new FileReader("src/test/resources/forTest.txt")){
            Lexer lexer = new Lexer(reader);
            assertEquals("public",lexer.readToken().getLexeme());
            assertEquals("class",lexer.readToken().getLexeme());
        }
        System.out.println("\n");
    }

    @Test
    public void testHasMoreTokensForEmptyFile() throws IOException {
        IReader reader = mock(IReader.class);
        when(reader.hasNext()).thenReturn(false);
        Lexer lexer = new Lexer(reader);
        assertEquals(false,lexer.hasMoreTokens());
        System.out.println("\n");
    }

    @Test
    public void testReadTokenWithBracketsSequence() throws IOException {
        IReader reader = mock(IReader.class);
        when(reader.hasNext()).thenReturn(true);
        when(reader.read()).thenReturn('{').thenReturn('}').thenReturn('{');
        Lexer lexer = new Lexer(reader);
        assertEquals("{",lexer.readToken().getLexeme());
        assertEquals("\n",lexer.readToken().getLexeme());
        assertEquals("}",lexer.readToken().getLexeme());
        System.out.println("\n");
    }

    @Test
    public void testReadTokenForStringLiteral() throws Exception {
        try(FileReader reader = new FileReader("src/test/resources/forSecondTest")){
            Lexer lexer = new Lexer(reader);
            assertEquals("{",lexer.readToken().getLexeme());
            assertEquals("\n",lexer.readToken().getLexeme());
            assertEquals("name",lexer.readToken().getLexeme());
            assertEquals("=",lexer.readToken().getLexeme());
            assertEquals("\" ;ffds{{ \";",lexer.readToken().getLexeme());
        }
        System.out.println("\n");
    }
}
