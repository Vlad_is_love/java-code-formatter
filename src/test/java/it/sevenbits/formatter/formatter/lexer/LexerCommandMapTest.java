package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.State;
import it.sevenbits.formatter.formatter.lexer.commands.ILexerCommand;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LexerCommandMapTest {

    @Test
    public void getCommandTest(){
        StringBuilder lexeme = new StringBuilder("public");
        List<String> lexemsLine = new ArrayList<>();
        Letter letter = new Letter(" ");
        LexerCommandMap lexerCommandMap = new LexerCommandMap(lexeme, lexemsLine, letter);
        State currentState = new State("LISTEN");
        List<ILexerCommand> commands = lexerCommandMap.getCommand(currentState, letter);
        for (ILexerCommand command : commands){
            command.execute();
        }
        assertEquals("wrong result", "public", lexemsLine.get(0));
    }
}
