package it.sevenbits.formatter.formatter.lexer;

import it.sevenbits.formatter.formatter.State;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class LexerStateMapTest {
    @Test
    public void getStateTest() throws IOException {
        LexerStateMap lexerStateMap = new LexerStateMap();
        Letter letter = new Letter("s");
        State currentState = lexerStateMap.getStartState();
        assertEquals("wrong result", new State("LISTEN"), lexerStateMap.getNextState(currentState, letter));
    }
}
