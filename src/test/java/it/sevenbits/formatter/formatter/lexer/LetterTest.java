package it.sevenbits.formatter.formatter.lexer;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LetterTest {
    @Test
    public void testGetTypeForSemicolon() {
        Letter letter = new Letter(";");
        assertEquals("wrong result", "SEMICOLON", letter.getType());
    }

    @Test
    public void testGetTypeForCurlyBracket() {
        Letter letter = new Letter("{");
        assertEquals("wrong result", "CURLY_BRACKET", letter.getType());
    }

    @Test
    public void testGetTypeForSymbol() {
        Letter letter = new Letter("u");
        assertEquals("wrong result", "SYMBOL", letter.getType());
    }
}
